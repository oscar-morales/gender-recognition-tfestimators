import os
import sys
import traceback

import numpy as np
import tensorflow as tf

from data_read import DataRead


class Estimator_Gender_model:
	def __init__(self):
		self._root_path = os.path.dirname(os.path.realpath(__file__))
		self._train_tf_files = "train_data/*.tfrecord"
		self._test_tf_files = "/test_data/*.tfrecord"
		self._BATCH_SIZE = 10
		self._IMAGE_SIZE = 128
		self._EPOCHS = 6600
		self._to_train = True
		self._model_output_directory = os.path.join(self._root_path,"gender_checkpoint_model")
		self._LEARNING_RATE = 1e-4
		self._dropout1 = 0.25
		self._dropout2 = 0.4
		self._ground_labels = []
		self._images = []


	def input_pipeline(self):
		""" Crea la estructura de canal de entrada de datos para la red unsando la API tf.data
		Retorna:
		iterador: iterator, iterador que permite obtener el siguiente elemento dentro del dataset
		"""
		if self._to_train:
			num_shards = count_files(self._train_tf_files)
			dataset = tf.data.Dataset.list_files(self._train_tf_files).shuffle(num_shards)
			dataset = dataset.interleave(tf.data.TFRecordDataset, cycle_length=num_shards)
			dataset = dataset.shuffle(buffer_size=8125, reshuffle_each_iteration=True)
		else:
			num_shards = count_files(self._test_tf_files)
			dataset = tf.data.Dataset.list_files(self._test_tf_files).shuffle(num_shards)
			dataset = dataset.interleave(tf.data.TFRecordDataset, cycle_length=num_shards)
			dataset = dataset.shuffle(buffer_size=4250, reshuffle_each_iteration=True)

		def decode_fn(tfrecord_file):
			""" Decodifica los elementos necesarios del archivo tfrecord para alimentar a la red
			Args:
				tfrecord_file: string, ruta de ubicación del archivo tfrecord --> /path/example/file.tfrecord
			Retorna:
				image: imagen jpeg
				label: int, identificador de la clase de la imagen.
			"""
			features = {
				'filename': tf.FixedLenFeature([], dtype=tf.string, default_value=''),
				'format': tf.FixedLenFeature([], dtype=tf.string, default_value=''),
				'height': tf.FixedLenFeature([], tf.int64),
				'width': tf.FixedLenFeature([], tf.int64),
				'channels': tf.FixedLenFeature([], tf.int64),
				'image': tf.FixedLenFeature([], dtype=tf.string, default_value=''),
				'label': tf.FixedLenFeature([], tf.int64)
			}
			sample = tf.parse_single_example(tfrecord_file, features)
			image = sample['image']

			with tf.name_scope('image_decode_jpeg', [ image ], None):
				image = tf.image.decode_jpeg( image, channels = 1 )
				image = tf.image.convert_image_dtype( image, dtype = tf.float32 )

			image_shape = tf.stack([sample['height'], sample['width'], sample['channels']])
			image = tf.reshape(image, image_shape)
			label = tf.cast(sample['label'], tf.int64)

			return {"image":image}, label

		dataset = dataset.repeat(self._EPOCHS)
		dataset = dataset.map(decode_fn, num_parallel_calls=2)
		dataset =  dataset.batch(self._BATCH_SIZE)
		dataset = dataset.prefetch(1)
		return dataset


	def cnn_model(self, features, labels, mode):
		input_layer = tf.reshape(features["image"], [-1, self._IMAGE_SIZE, self._IMAGE_SIZE, 1])
		with tf.name_scope("ConvPoolLayer1"):
			conv1 = tf.layers.conv2d(inputs = input_layer, filters = 32, kernel_size = [3,3], strides=1, padding = "SAME", activation = tf.nn.relu)

#			conv2 = tf.layers.conv2d(inputs = conv1, filters = 32, kernel_size = [3,3], strides=1, padding = "SAME", activation = tf.nn.relu)

			pool1 = tf.layers.max_pooling2d(inputs=conv1, pool_size=[2,2], strides=2, padding = "SAME")
#			dropout1 = tf.layers.dropout(pool1, self._dropout1, training=mode == tf.estimator.ModeKeys.TRAIN)
		with tf.name_scope("ConvPoolLayer2"):
			conv3 = tf.layers.conv2d(inputs = pool1, filters = 64, kernel_size=[3,3], strides=1, padding="SAME",activation=tf.nn.relu)

#			conv4 = tf.layers.conv2d(inputs = conv3, filters = 64, kernel_size=[3,3], strides=1, padding="SAME",activation=tf.nn.relu)

			pool2 = tf.layers.max_pooling2d(inputs=conv3, pool_size=[2,2], strides=2, padding = 'SAME')
#			dropout2 = tf.layers.dropout(pool2, self._dropout1, training=mode == tf.estimator.ModeKeys.TRAIN)
		with tf.name_scope("ConvPoolLayer3"):
			conv5 = tf.layers.conv2d(inputs = pool2, filters = 128, kernel_size = [3,3], strides=1, padding = "SAME", activation = tf.nn.relu)

#			conv6 = tf.layers.conv2d(inputs = conv5, filters = 128, kernel_size = [3,3], strides=1, padding = "SAME", activation = tf.nn.relu)

			pool3 = tf.layers.max_pooling2d(inputs=conv5, pool_size=[2,2], strides=2, padding='SAME')
			#dropout3 = tf.layers.dropout(pool3, self._dropout1, training=mode == tf.estimator.ModeKeys.TRAIN)
		with tf.name_scope("ConvPoolLayer4"):
			conv7 = tf.layers.conv2d(inputs = pool3, filters = 256, kernel_size = [3,3], strides=1, padding="SAME", activation=tf.nn.relu)

			pool4 = tf.layers.max_pooling2d(conv7, pool_size = [2, 2], strides = 2, padding="SAME")
#			#dropout4 = tf.layers.dropout(pool4, self._dropout1, training=mode == tf.estimator.ModeKeys.TRAIN)

		with tf.name_scope("ConvPoolLayer5"):
			conv8 = tf.layers.conv2d(inputs = pool4, filters = 512, kernel_size = [3,3], strides=1, padding="SAME", activation=tf.nn.relu)

			pool5 = tf.layers.max_pooling2d(conv8, pool_size = [2, 2], strides = 2, padding="SAME")

		with tf.name_scope("Flatten"):
#			shape = conv8.get_shape().as_list()
#			flat = tf.reshape(conv8, [-1, 8 * 8 * 512])
			flat_layer = tf.layers.flatten(pool5)

		with tf.name_scope("DenseLayer"):
			full1 = tf.layers.dense(inputs=flat_layer, units=1024, activation=tf.nn.relu)
			if self._to_train:
				full_dropout = tf.layers.dropout(inputs=full1, rate=self._dropout2, training=mode == tf.estimator.ModeKeys.TRAIN)
			else:
				full_dropout = full1
			full2 = tf.layers.dense(inputs=full_dropout, units=1024, activation=tf.nn.relu)
#			full_dropout2 = tf.layers.dropout(inputs=full2, rate=self._dropout2, training=mode == tf.estimator.ModeKeys.TRAIN)

		with tf.name_scope("logits"):
			logits = tf.layers.dense(inputs=full2, units=2)

		try:
			global_step = tf.train.get_global_step()
			predicted_logit = tf.argmax(input=logits, axis=1,output_type=tf.int32)
			probabilities = tf.nn.softmax(logits)

			#predictions
			predictions = {"predicted_logits" : predicted_logit,"probabilities" : probabilities}

			if mode == tf.estimator.ModeKeys.PREDICT:
				return tf.estimator.EstimatorSpec(mode=mode, predictions=predictions)

			with tf.name_scope('loss'):
				cross_entropy = tf.losses.sparse_softmax_cross_entropy(
					labels=labels, logits=logits, scope='loss')
				tf.summary.scalar('loss', cross_entropy)

			with tf.name_scope('accuracy'):
				accuracy = tf.metrics.accuracy(
					labels=labels, predictions=predicted_logit, name='acc')
				tf.summary.scalar('accuracy', accuracy[1])


			#evaluation
			if mode == tf.estimator.ModeKeys.EVAL:
				return tf.estimator.EstimatorSpec(
					mode=mode,
					loss=cross_entropy,
					eval_metric_ops={'accuracy/accuracy':accuracy},
					evaluation_hooks=None)

			optimizer = tf.train.AdamOptimizer(self._LEARNING_RATE)
			train_ops = optimizer.minimize(cross_entropy,global_step=global_step)

			#for a visuzalization using tensorboard every 100 iterations
			train_hook_list = []
			train_tensors_log = {'accuracy':accuracy[1],
					'loss':cross_entropy,
					'global_step':global_step}
			train_hook_list.append(tf.train.LoggingTensorHook(tensors=train_tensors_log, every_n_iter=100))

			if mode == tf.estimator.ModeKeys.TRAIN:
				return tf.estimator.EstimatorSpec(
					mode=mode,
					loss=cross_entropy,
					train_op=train_ops,
					training_hooks=train_hook_list)
		except Exception:
			print(f"Error-->{sys.exc_info()}")


	def get_images_for_inference(self):
		"""
		Provee a la red un método de entrada para la prediccion en imagenes no vistas
		"""
		if not os.path.exists(source_dirname):
			return None
		if self._type_of_input == "name":
			relative_path_images, labels = dataRead.read_by_name(source_dirname)
		elif self._type_of_input == "directories":
			relative_path_images, labels = dataRead.read_by_directories(source_dirname)
		else:
			relative_path_images, labels = dataRead.read_by_random(source_dirname)
		self._images = relative_path_images
		self._ground_labels = labels

		try:
			dataset = tf.data.Dataset.from_tensor_slices((relative_path_images, labels))
			batch_size = 1
			dataset = dataset.map(self.load_and_preprocess_image)
			dataset = dataset.batch(batch_size).prefetch(None)
			return dataset
		except IOError:
			print(f"Error-->{sys.exc_info()}")
			return None


	def preprocess_image(self, image, label):
		image = tf.io.decode_jpeg(image, channels = 1)
		image = tf.image.resize_images(image, [self._IMAGE_SIZE, self._IMAGE_SIZE])
		return {"image":image}, label


	def load_and_preprocess_image(self, path, labels):
		image = tf.read_file(path)
		return self.preprocess_image(image, labels)


	def _execute_inference(self, classifier):
		"""Executes the inference process
		The output is given in the form [man, woman] where 1 means such
		class had the highest probability, example: [1, 0] --> man,
		[0,1]--> woman
		"""
		try:
			predicted_classes = classifier.predict(input_fn=lambda:self.read_file())
			labels_predicted = []

			for index, prediction in enumerate(predicted_classes):
				labels_predicted.append(prediction["predicted_logits"])
				dataRead.save_image(self._images[index], prediction["predicted_logits"], index)
		except tf.errors.OutOfRangeError:
			pass
		print(f"Ground labels {self._ground_labels} \nInferred values {labels_predicted}")
		print("Model used {self._model_output_directory}")
		self._evaluate_predictions(labels_predicted)


	def _evaluate_predictions(self, labels_predicted):
		"""Evaluate the predictions made by the model, produced with the
		current architecture based on the ouput of execute inference
		method and the truth labels of such images used.
		"""
		assert len(self._ground_labels) == len(labels_predicted), "length of predicted labels does not match with the number of images."

		correct_predictions = 0
		for index in range(len(self._ground_labels)):
			if self._ground_labels[index] == labels_predicted[index]:
				correct_predictions += 1
		accuracy = (correct_predictions/len(self._ground_labels)) * 100
		print("prediction accuracy is {format(accuracy, '5.2f')}")


	def train_model(self, classifier):
		try:
			classifier.train(input_fn=lambda:self.input_pipeline(), steps=self._EPOCHS)
			self._to_train = False
			evaluation = classifier.evaluate(input_fn=lambda:self.input_pipeline(), steps=16)
			print(evaluation)
		except tf.errors.OutOfRangeError:
			pass
		except IOError:
			print(f"error-->{sys.exc_info()}")


	def main(self):
		try:
			classifier = tf.estimator.Estimator(
				model_fn=self.cnn_model_fn, model_dir=self._model_output_directory)
		except IOError:
			print("Error-->{0}".format(sys.exc_info()))


if __name__ == '__main__':
	tf.logging.set_verbosity(tf.logging.INFO)
	Estimator_Gender_model().main()
#tf.app.run(main)
