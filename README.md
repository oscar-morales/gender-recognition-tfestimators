*The project is passing throughout a complete  re-write, many files are not working properly see more in  [Contributing](#Contributing).*

# Gender Recognition Tfestimators

Python (**CLI**) application for gender classification based on grey scale images of human faces using [Tensoflow tfestimators](https://www.tensorflow.org/api_docs/python/tf/estimator/Estimator), the images are subjected to different pre-processing techniques before being used for training and testing. The application can make predictions on a group of images or on a sigle one in command line, see usage for more.

The application uses opencv [haarcascade](https://github.com/opencv/opencv/tree/master/data/haarcascades) face detector (see below why) like first aproach to detect faces inside the image but if this detector fails, it uses [dlib](http://dlib.net/) **HOG + Linear SVM (dlib.get_frontal_face_detector())** like a second face detector mechanism. Both mechanism were chosen due to the lack of a GPU to make more complex facial recognition techniques, where the use of a GPU is more convenient to compute the data in terms of [higher speed an efficiency](https://www.analyticsvidhya.com/blog/2017/05/gpus-necessary-for-deep-learning/) (such as face recognition in real time with videos).

The cnn architecture used in this project was inspired from the [VGG16 & CGG19](https://arxiv.org/abs/1409.1556) architectures, *the architecture used here is not a faithful copy from the previously mentioned architectures since they were simply used as a guide and it was modified experimentally* (this is why you see comments in the network arhitecture code).

---

## Install

Clone the project using git:

```git
git clone https://gitlab.com/oscar-morales/gender-recognition-tfestimators.git
```

### Method 1
You can execute the **requirements.txt** file like so:

```
python -m pip <pip arguments>
```

### Method 2
If you're using conda (like me) you can execute the following [script](https://gist.github.com/luiscape/19d2d73a8c7b59411a2fb73a697f5ed4) inside your enviroment:

```
while read requirement; do conda install --yes $requirement; done < requirements.txt
```

If you don't know how to create an enviroment with conda, you can ckeck out [this](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html).

Also you can create an identical enviroment from requirements.txt:

```bash
conda create --name <enviroment-name> --file requirements.txt
```

<details open>
<summary>Conda troubleshooting</summary>
<br>
If you see an error like:

PackagesNotFoundError: The following packages are not available from current channels: ...


Write the [following](https://stackoverflow.com/a/48509426/7818641) line in the cmd/terminal withing your active enviroment:

```bash
conda config --append channels conda-forge
```
</details>

**Don't forget to download [haarcascades](https://github.com/opencv/opencv/tree/master/data/haarcascades) xml file from opencv**, create a folder inside your project (*gender-recognition-tfestimator/main/haarcascades*) with the same name (haarcascades) and inside put the **haarcascade_frontalface_default.xml**.

---

## Usage

Once you have downloaded everything, you should put your directories for training a testing the network in the following form:

```bash
train_images/
├── man
│   └── image_x.jpg
└── woman
    └── image_x.jpg

test_images/
├── man
│   └── image_x.jpg
└── woman
    └── image_x.jpg
```

Now in order to pre-process the images to create your dataset you should execute the file in **main/preprare_image.py** passing two arguments:

```bash
python main/preprare_image.py train_directory_path output_directory_name
```
*output_directory_name* it's gonna be created in the same path where the file *preprare_image.py* resides. Also you can pass the flag *-h* to get information about the arguments:

```bash
python main/preprare_image -h
```

Next you can create your tfrecord files with this [script](https://github.com/yeephycho/tensorflow_input_image_by_tfrecord). Or create an input pipeline using the newly features of [tf.data](https://www.tensorflow.org/guide/data) and create a folder where all your train/test tf files will live.

Set the variables inside *main/estimator_custom_model.py* with the path of your folders with your tf files, the file expects by default that the directories are inside where the file in running:

```python
self._train_tf_files = "train_data/"
self._test_tf_files = "test_data/"
```
And set the variable *output_directory_model* where the generated model will be saved. If you do not modify this variable the model will be saved in the same path where *estimator_custom_model.py* resides.

And finally you can execute the file *main/estimator_custom_model.py*, the predictions will generate the following directory structure:

```bash
inference/
├── man
│   └── man_number.jpg
└── woman
    └── woman_number.jpg
```

---

## Contributing

If you want to contribute to the project fork the repository and make a Merge request pointing to the branch **Development**. Be sure to follow the [PEP 8](https://www.python.org/dev/peps/pep-0008/) python coding style and please instead of using *double underscores* for instance variables use only **one underscore**.

You should not do this:
```python
self.__instance_variable = some_value
```

Instead do this:
```python
self._instance_variable = some_value
```

Test your snipped of code before make a Merge request and include your test for your contribution in your merge request, use [Unit Testing framework](https://docs.python.org/3/library/unittest.html) which is included with python.

If you find a bug or a problem please use Gitlab [issue tracker](https://gitlab.com/oscar-morales/gender-recognition-tfestimators/-/issues) to report it.


Due to an *big update* for the project to migrating it to use [Tensorflow 2](https://www.tensorflow.org/api_docs/python/tf), I'm working to improve the code as much as I can, for such reason many files maybe are not working properly or there are references to code that does not exists anymore. If you want to fix some of this bad references you will be welcome or if you want to update some part of the code to use Tensorflow 2, you will be welcome too.

Also if you see some bad practices in the code, I invite you to contribute to improve such code instead of complaining about that.
> “Experience is the name everyone gives to their mistakes.” – Oscar Wilde

---

## Licence

This project uses the [Apache License 2.0](https://gitlab.com/oscar-morales/gender-recognition-tfestimators/-/blob/main/LICENSE)
