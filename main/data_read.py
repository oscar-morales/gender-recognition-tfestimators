import os
import sys
import traceback
import random

import cv2 as cv

from preprare_image import PrepareImage

class DataRead:

	def __init__(self):
		"""Initialize basic information in order to offer a model pipeline

		The script sets up a dictionary in order to generate the image
		labels for inferred images, paths to save them and helper classes."""
		self._labels = {'hombre': 0, 'mujer': 1}
		self._inferencia_log_file = "inferencia_log.txt"
		self._root_path = os.path.dirname(os.path.realpath(__file__))
		self._prepareImage = PrepareImage()


	def _get_class_label(self, class_name):
		"""Return the corresponding class name label (0|1)"""
		return self._labels[class_name]


	def count_tfrecords_files(self, source_directory):
		"""Return number of tfrecord files inside a directory

		The method only cares about files with the .tfrecord extention.
		Args:
			source_directory: string, path of the directory containing the
			tfrecord files.
		Return:
			int: number of files inside of the directory.
		"""
		number_of_files = 0
		for tf_file in os.listdir(os.path.dirname(source_directory)):
			if tf_file.lower().endswith('.tfrecord'):
				number_of_files += 1
		return number_of_files


	def read_by_name(self, source_dirname):
		"""Return a list of paths and labels based on the image name.

		Gives to the network a pipeline for prediction on unseen images inside a
		directory. Unlike read_by_directories method, this method returns the
		labels for each image based in the image's name, the image name should be
		in the form: woman_0: mujer_0.jpg, man_0: hombre_0.jpg. Where '_' is the
		delimiter that separates the class of the image from the rest of the image
		name.
		Images and directories should be in the following structure:
			..directory_with_images/woman_0.jpg
			..directory_with_images/man_0.jpg
		If the directory does not exists or the directory does not contain any
		images, the returned lists will be empty.
		Args:
			source_dirname: string, directory path containing the images.
		Return:
			list: string, image paths.
			list: int, image labels
		"""
		if not os.path.exists(source_dirname):
			print(f"The directory does not exists or the path is wrong -->{source_dirname}")
			return [], []

		absolute_path_images = []
		labels = []
		for image in os.listdir(source_dirname):
			if not prepapreImage.allowed_image_extention(image):
				print(f"Image format is not valid: {image}... Skipping image.")
				continue
			labels.append(self._get_class_label(os.path.basename(image).split('_')[0]))
			absolute_path_images.append(os.path.join(source_dirname, image))

		return absolute_path_images, labels


	def read_by_directories(self, source_dirname):
		"""Return a list of paths and labels based on the directory name.

		Gives to the network a pipeline for prediction on unseen images.
		The Images and directories should be in the following structure:
		parent_directory:
			..directory_class_1/image_class1_x.jpg
			..directory_class_2/image_class2_x.jpg
		The labels are generated with the name of the directory inside
		the parent directory->'directory_class_1', that means: the image
		inside 'directory_class_1' will have the corresponding label of
		'directory_class_1'.
		If the directory does not exists or the directories aren't in the
		specified structure or the directory does not contain any images,
		the returned lists will be empty.
		Args:
			source_dirname: string, directory path containing the images.
		Return:
			list: string, image paths.
			list: int, image labels.
		"""
		if not os.path.exists(source_dirname):
			return [], []

		absolute_path_images = []
		labels = []
		for classes in os.listdir(source_dirname):
			if len(classes) > 0 and classes in self._prepareImage.classes:
				source_dirname_gender = os.path.join(source_dirname, class_name)
				for image in os.listdir(source_dirname_gender):
					if not prepareImage.allowed_image_extention(image):
						print(f"Image format is not valid: {image}... Skipping image.")
						continue
					absolute_path_images.append(os.path.join(source_dirname_gender, image))
					labels.append(self._get_class_label(class_name))

		return absolute_path_images, labels


	def read_by_random(self, source_dirname):
		"""Return a list of paths and random labels from the images.

		Gives to the network a pipeline for prediction on unseen images.
		This method generates labels for each image with random values of:
		0 (for men) and 1 (for women), assuming a real case where is unknown
		the class of the image to be inferred.
		The images and directories should be in the following structure:
			..directory_with_images/image_x.jpg.
		If the directory does not exists or the directory does not contain any
		images, the returned lists will be empty.
		Args:
			source_dirname: string, directory path with images.
		Return:
			list: string, image paths.
			list: int, image labels.
		"""
		if not os.path.exists(source_dirname):
			return [], []

		absolute_path_images = []
		labels = []
		for image in os.listdir(source_dirname):
			if not prepareImage.allowed_image_extention(image):
				print(f"Image format is not valid: {image}... Skipping image.")
				continue
			absolute_path_images.append(os.path.join(source_dirname, image))
			labels.append(random.randrange(0, 2))
		return absolute_path_images, labels


	def save_image(self, image_path, predicted_label, image_index):
		"""Save the image on directory class based on performed prediction

		The method saves the iamge in the directory of it's corresponding
		class:
			inferencia/class_1/image_to_save.jpg
			inferencia/class_2/image_to_save.jpg
		Args:
			image_path: string, image path to save on disk.
			predicted_label: int; inferred class by the model 0|1.
			image_index: int; number of the image (which was inferred).
		"""
		if os.path.exists(image_path):
			image_name = os.path.basename(image_path)
			cv_image = cv.imread(image_path, 0)
			inference_directory = os.path.join(self._root_path,"/inferencia/")

			if predicted_label == 1:
				inference_directory_gender = self._create_dirs(inference_directory, "mujer")
				new_image_name = ".".join(["_".join([self._prepareImage.classes[1], str(image_index)]), self._prepareImage.allowed_image_extention[0]])
				try:
					cv.imwrite(os.path.join(inference_directory_gender, new_image_name), cv_image)
				except IOError:
					print(f"An error ocurred while trying to save the image->{sys.exc_info()}")
				PrepareImage().write_on_file(image_name, new_image_name, self._inferencia_log_file)
			elif predicted_label == 0:
				inference_directory_gender = self._create_dirs(inference_directory, "hombre")
				new_image_name = ".".join(["_".join([self._prepareImage.classes[0], str(image_index)]), self._prepareImage.allowed_image_extention[0]])
				try:
					cv.imwrite(os.path.join(inference_directory_gender, new_image_image), cv_image)
				except IOError:
					print(f"An error ocurred while trying to save the image->{sys.exc_info()}")
				PrepareImage().write_on_file(image_name, new_image_image, self._inferencia_log_file)
		else:
			print(f"The file does not exists or the path is wrong-->{full_image_path}")


	def _create_dirs(self, directory_path, gender):
		gender_directory = os.path.join(directory_path, gender)
		if not os.path.exists(gender_directory):
			os.makedirs(gender_directory)

		return gender_directory
