import os
import sys
import traceback
import random
import shutil
import argparse

import cv2 as cv
import dlib
import numpy as np
from PIL import Image
from skimage import io


class PrepareImage(object):

	def __init__(self):
		"""Initilize all paths for the generated directories and log files.


		The script checks if harrcascade folder/file is in the same location
		where the script is executed, if not, a	FileNotFoundError exception is
		raised.

		The the script also generates the following directories:
		destination_directry(expected as argument for the script): path where
		train/test images will be saved after pre-processing them.
		error_images: path where all the images with errors will be saved.
		(these images are not considered in 'destination_directory').
		negative_faces: path where all images with no faces detected by opencv
		or dlib will be saved.

		Generated log files:
		processed_images.txt: file with all the processed image names. (images
		included in 'destination_directory'.)
		images_without_faces.txt: file with all the image names without a
		detected face on it.
		"""
		self._root_path = os.path.dirname(os.path.realpath(__file__))
		self._source_directory = self._root_path
		self._destination_directory = self._root_path
		self._verbose = False
		self._get_args()
		self._path_error_images= os.path.join(self._root_path,"error_images")
		self._nofaces_dirname = os.path.join(self._root_path,"negative_faces")
		self._face_cascade = None
		if not os.path.exists(os.path.join(self._root_path, "haarcascades/")):
			raise FileNotFoundError(f"haarcascades folder does not exists in the current directory {self._root_path}")
		else:
			self._face_cascade = cv.CascadeClassifier("./haarcascades/haarcascade_frontalface_default.xml")
		self.classes = ("hombre", "mujer")
		self.allowed_image_extention = ("jpg", "jpeg", "png")
		self._full_image = []
		self._scale_factor = 1.3
		self._minimun_neighbors = 3
		self._minimun_size = (20, 20)
		self._IMG_SIZE = 128
		self._processed_images_log = os.path.join(self._root_path, "processed_images.txt")
		self._images_with_no_faces_log = os.path.join(self._root_path, "images_without_faces.txt")


	def _get_args(self):
		"""Define the necessary arguments to run the script."""
		parser = argparse.ArgumentParser(description="Program that pre-process images with different techniques and\
			looks for faces. The script expects a source_directory in order to preprocess the images \
			there and save them in a destination_directory (also expected as input). if destination_directory\
			already exitsts the prorgram will ask the user if the path could be replaced, if path not exists,\
			the path will be created.")
		parser.add_argument("source_directory", help="Directory containing the images in order to create the dataset for training\
			/testing the network.")
		parser.add_argument("destination_directory", help="Directory where the processed images will be saved.")
		parser.add_argument("-v", "--verbosity", action="store_true", help="Display pre-processing process information.")
		args = parser.parse_args()
		self._valid_arguments(args)


	def _valid_arguments(self, args):	
		"""Checks if passed paths are valid and sets the instance variables
		destination_directory and source_directory if they're valid.
		"""
		if args.source_directory:
			if not os.path.exists(args.source_directory):
				raise FileNotFoundError(f"The directory {args.source_directory} does not exists.")
			else:
				self._source_directory = args.source_directory
		if args.destination_directory:
			if os.path.exists(args.destination_directory):
				response = input(f"The directory {args.destination_directory} already exists\n do you want to replace it's content? yes [y], no [n] ").strip()
				if (response.lower() == 'y'):
					self._destination_directory = args.destination_directory
				elif (response.lower() == 'n'):
					sys.exit("The directory already exists and can't be overrided")
				else:
					sys.exit("The option does not exists\n Aborting operation.")
			else:
				self._destination_directory = args.destination_directory
		if args.verbosity:
			self._verbose = True
			print("Showing all the process information.")


	def _create_directory(self, directory_path):
		if not os.path.exists(directory_path):
			os.makedirs(directory_path)


	def _verify_image_has_no_error(self, image_path):
		"""Checks an image for errors."""
		try:
			sky_image = io.imread(image_path)
			return True
		except IOError:
			print(f"Couldn't read image: stack {sys.exc_info()}")
			return False


	def allowed_image_extention(self, image_path):
		"""Return if image extention is from a valid extention (jpg, jpeg, png)."""
		return os.path.basename(image_path).lower().endswith(self.allowed_image_extention)


	def _is_gray_scale(self, image_path):
		"""Return if the image is graycale traversing each pixel in the	image."""
		image = Image.open(image_path).convert('RGB')
		image_width, image_height = image.size
		for i in range(image_width):
			for j in range(image_height):
				r, g, b = image.getpixel((i, j))
				if r != g != b:
					return False
		return True


	def _get_correct_color_format(self, image_path, apply_insto_enhancement):
		"""Given an image path, opecv object loads each image with the correct
		color format.
		Args:
			image_path: string, path of the image,
			apply_insto_enhancement: boolean, (optional argument) if wished
			to apply histo enhancement to the image after opencv loads the
			image.
		Return:
			opencv_object, correct color format image (RGB or grayscale).
		"""
		if self._is_gray_scale(image_path):
			return cv.imread(image_path, 0)
		else:
			cv_image = cv.imread(image_path, 1)
			if apply_insto_enhancement:
				return self._histo_enhancement(cv_image)
			return cv_image


	def _face_detector(self, image_path, class_name, destination_dirname, counter):
		"""Detects faces inside an image using facecascade detector.
		Args:
			image_path: string; path of the image.
			classname: string, class name that the image belongs.
			destination_dirname: string, directory path were the images will
			be saved.
			counter: integer, counter that will be part of image's name
			-> hombre_contador.jpg
		Return:
			boolean: if the given image contains a face.
		"""
		opencv_image = self._resize_keep_aspect_ratio(self._get_correct_color_format(image_path, False))
		if opencv_image is not None:
			try:
				faces_on_image = self._face_cascade.detectMultiScale(opencv_image, scaleFactor=1.3, minNeighbors=3, minSize=(20, 20))

				if len(faces_on_image) <= 0:
					return False

				for (x, y, w, h) in faces_on_image:
					face_cuts = opencv_image[y:y + h, x:x + w]
					if not self._is_gray_scale(image_path):
						face_cuts = cv.cvtColor(face_cuts, cv.COLOR_BGR2GRAY)
					self._save_image(face_cuts, class_name, destination_dirname, counter)
				return True
			except:
				print(f"face_detector: Error al procesar la imagen, info--> {sys.exc_info()}")
				return False
		return False



	def _dlib_detector(self, image_path, class_name, destination_dirname, counter):
		"""Detects faces inside an image using dlib face detector.

		It's used like a second mechanism for face detection if opencv detector
		fails to detect a face inside an image.
		Args:
			image_path: string; path of the image.
			class_name: string; class name that the image belongs.
			destination_dirname: string, directory path were the images will
			be saved.	
			counter: int; counter that will be part of the name of the image.
			--> mujer_contador.jpg
		Return:
			boolean: if the given image contains a face.
		"""
		image = self._resize_keep_aspect_ratio(self._get_correct_color_format(image_path, False))
		try:

			face_detector_object = dlib.get_frontal_face_detector()
			detected_faces = face_detector_object(image, 1)

			if len(detected_faces) <= 0:
				return False

			for i, face_rectangle in enumerate(detected_faces):
				image_to_crop = Image.open(image_path).convert('RGB')
				crop_area = (face_rectangle.left(), face_rectangle.top(), face_rectangle.right(), face_rectangle.bottom())

				cropped_image = image_to_crop.crop(crop_area)
				crop_size = (image.shape[0], image.shape[1])
				cropped_image.thumbnail(crop_size)
				#convert the image back from PIL.Image to opencv
				opencvImage = cv.cvtColor(np.array(cropped_image), cv.COLOR_RGB2GRAY)
				self._save_image(opencvImage, class_name, destination_dirname, counter)
			return True

		except:
			print(f"Dlib detector: Error al procesar la imagen, info -->{sys.exc_info()}, {image_path}")
			return False


	def _resize_keep_aspect_ratio(self, opencv_image):
		"""Return a resized opencv image with its aspect ratio keeped."""
		try:
			#(height, width)
			old_image_size = opencv_image.shape[:2]

			image_ratio = float(self._IMG_SIZE)/max(old_image_size)
			new_image_size = ([int(x*image_ratio) for x in old_image_size])

			#new_size should be in the form: (width, height)
			opencv_image = cv.resize(opencv_image, (new_image_size[1], new_image_size[0]))

			delta_width = self._IMG_SIZE - new_image_size[1]
			delta_height = self._IMG_SIZE - new_image_size[0]
			top, bottom = delta_height//2, delta_height - (delta_height//2)
			left, right = delta_width//2, delta_width - (delta_width//2)

			color = [0, 0, 0]
			new_image = cv.copyMakeBorder(opencv_image, top, bottom, left, right, cv.BORDER_CONSTANT, value=color)
			return new_image
		except IOError:
			print(f"Resize keep aspect: Hubo errores al realizar las operaciones sobre la imagen {sys.exc_info()}")
			return None


	def _histo_enhancement(self, opencv_image):
		"""Return an opencv image with histogram enualization applied."""
		R, G, B = cv.split(opencv_image)
		output_R = cv.equalizeHist(R)
		output_G = cv.equalizeHist(G)
		output_B = cv.equalizeHist(B)
		output_image = cv.merge((output_R, output_G, output_B))
		return output_image


	def resize_images_by_directories(self):
		"""Given a directory in the form:
			../data/hombre/img-x.jpg
			../data/mujer/img-x.jpg
		it calls the methods to pre-process the images: finding a face, cutting
		it, resizing it and chaning it's channel to 1.
		Generates the directories:
			detination_directory/mujer/mujer-x.jpg
			destination_directory/hombre/hombre-x.jpg
			destination_directory/mujer/mujer-x.jpg
			destination_directory/hombre/hombre-x.jpg
			"""
		if self._verbose:
			print("Working...")
		for class_name in os.listdir(self._source_directory):
			if len(class_name) > 0 and class_name.lower() in self.classes:
				counter = 0
				no_faces_counter = 0
				self.write_on_file(self._source_directory, class_name, self._processed_images_log)
				self.write_on_file(self._source_directory, class_name, self._images_with_no_faces_log)
				destination_gender_directory = os.path.join(self._destination_directory, class_name)
				source_dirname_gender = os.path.join(self._source_directory, class_name)
				for image in os.listdir(source_dirname_gender):
					image_path = os.path.join(source_dirname_gender, image)
					if not self._verify_image_has_no_error(image_path):
						print(f"la imagen contiene errores y será eliminada-descartada {image_path}")
						os.remove(image_path)
						continue

					if self._face_detector(image_path, class_name, destination_gender_directory, counter):
						self.write_on_file(os.path.basename(image), class_name+"_"+str(counter)+self.allowed_image_extention[0], self._processed_images_log)
						counter += 1
					elif self._dlib_detector(image_path, class_name, destination_gender_directory, counter):
						self.write_on_file(os.path.basename(image), class_name+"_"+str(counter)+self.allowed_image_extention[0], self._processed_images_log)
						counter += 1
					else:
						nofaces_path = os.path.join(self._nofaces_dirname, class_name)
						self._create_directory(nofaces_path)
						self.write_on_file(os.path.basename(image), os.path.basename(image), self._images_with_no_faces_log)
						try:
							shutil.move(image_path, nofaces_path)
						except OSError:
							print(f"resize images: couldn't move image see -->{sys.exc_info()}")
						no_faces_counter += 1
				if self._verbose:
					print(f"{no_faces_counter} images without faces finded.")
			else:
				print(f"Directory structure should be in the form:\n parent_directory/..\
					\ndirectory_class1/image_x.jpg \n directory_class2/image_x.jpg \
					\n directory {self._source_directory} is not in this form.")
		if self._verbose:
			print(f"{no_faces_counter} images without faces \n Done!")


	def resize_one_level_images(self, name_included = False):
		"""Calls the methods to pre-process the images

		The corresponding tasks are: finding a face, cutting it, resizing it and
		chaning it's channel to 1. Images must be inside a directory with it's
		corresponding class name like it's image name.	
		Args:
			name_included: boolean; specifies that the class name of each image is
			included in its name (True) otherwise (False).	
		"""
		if self._verbose:
			print("Working...")
		counter = 0
		positive_faces = 0
		for image in os.listdir(self._source_directory):
			image_path = os.path.join(self._source_directory, image)
			if not self._verify_image_has_no_error(image_path):
				print(f"The image contains errors and will not be processed: {image}")
				continue

			image_class_name = self._get_image_name(image) if name_included else image[:image.find(".")]
			if self._face_detector(image_path, image_class_name, self._destination_directory, counter):
				self.write_on_file(os.path.basename(image), image_class_name+"_"+str(counter)+self.allowed_image_extention[0], self._processed_images_log)
				counter += 1
			elif self._dlib_detector(image_path, image_class_name, self._destination_directory, counter):
				self.write_on_file(os.path.basename(image), image_class_name+"_"+str(counter)+self.allowed_image_extention[0], self._processed_images_log)
				counter += 1
			else:
				self._create_directory(self._nofaces_directory)
				try:
					shutil.move(image_path, self._nofaces_directory)
				except OSError:
					print(f"Couldn't move image see -->{sys.exc_info()}")
		if self._verbose:
			print(f"{counter} found faces.")


	def _save_image(self, full_image, class_name, destination_dirname, counter):
		"""Saves grayscale face images on disk.
		
		Args:
			full_image: opencv_object; image's opencv object.
			class_name: string; class name which the face image belongs.
			destination_dirname: string; directory path where the image will be
			saved.
			counter: Integer; image number, it's based on the number of images
			that the directory ..source_directory has.	
		"""
		self._create_directory(destination_dirname)
		resized_image = self._resize_keep_aspect_ratio(full_image)
		try:
			cv.imwrite(os.path.join(destination_dirname, class_name + "_" + str(counter)+".jpg"), resized_image)
		except IOError:
			print(f"save_image: Error al intentar gaurdar la imagen: {sys.exc_info()}")


	@staticmethod
	def write_on_file(basename_img, newbasename_img, log_txt_name):
		"""Creates a log file with the previous image name and its new name in
		the form: newbasename_img*basename_img
		Args:
			basename_img: string; original image name.
			newbasename_img: string; new name of the image
			log_name: string; name of the log file to create. e.g
			processed_images.txt"""

		assert log_txt_name.lower().endswith(".txt"), "el formato de archivo debe ser .txt"

		if os.path.exists(log_txt_name):
			with open(log_txt_name, "a") as log_file:
				log_file.write(newbasename_img+"*"+basename_img+"\n")
		else:
			with open(log_txt_name, "w") as log_file:
				log_file.write(newbasename_img+"*"+basename_img+"\n")


	def _get_image_name(self, image_class_name):	
		"""Return the classname (hombre|mujer) according to the name that the
		image has: imagen-mujer234.jpg -> return mujer.
		"""
		if  os.path.basename(image_class_name) in self.classes:
			return self.classes[self.classes.index(image_class_name)]
		return os.path.basename(image_class_name)
